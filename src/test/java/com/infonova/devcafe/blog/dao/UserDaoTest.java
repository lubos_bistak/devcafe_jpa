package com.infonova.devcafe.blog.dao;

import com.infonova.devcafe.blog.AbstractTest;
import com.infonova.devcafe.blog.model.Address;
import com.infonova.devcafe.blog.model.User;
import com.infonova.devcafe.blog.model.UserPreview;
import org.joda.time.DateMidnight;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.util.List;

import static org.junit.Assert.*;

@Transactional
public class UserDaoTest extends AbstractTest {

	@Autowired
	private UserDao userDao;

	@Test
	public void saveUser() {
		String primaryKey = "username";
		String eMail = "mail@me.com";
		User user = createUser(primaryKey, eMail);

		userDao.saveEntity(user);

		User dbUser = em.find(User.class, primaryKey);
		assertEquals(dbUser.getUsername(), user.getUsername());
		assertEquals(dbUser.getFirstName(), user.getFirstName());
		assertEquals(new DateMidnight().toDateTime(),
				user.getRegistrationDate());
	}

	@Test(expected = EntityExistsException.class)
	public void saveDuplicateUser() {
		User user1 = createUser("firstUser", "mail@me.com");
		User user2 = createUser("firstUser", "mail@me.com");

		userDao.saveEntity(user1);
		userDao.saveEntity(user2);
	}

	@Test
	public void findByFirstName() {
		User user1 = createUser("firstUser", "mail@me.com");
		User user2 = createUser("secondUser", "mail2@me.com");
		User user3 = createUser("thirdUser", "mail3@me.com");
		user3.setFirstName("anotherName");

		userDao.saveEntity(user1);
		userDao.saveEntity(user2);
		userDao.saveEntity(user3);

		List<User> foundUsers = userDao.findByFirstName("firstName");
		assertEquals(2, foundUsers.size());
	}

	@Test
	public void findByLastName() {
		User user1 = createUser("firstUser", "mail@me.com");
		User user2 = createUser("secondUser", "mail2@me.com");
		User user3 = createUser("thirdUser", "mail3@me.com");
		user3.setLastName("anotherName");

		userDao.saveEntity(user1);
		userDao.saveEntity(user2);
		userDao.saveEntity(user3);

		List<User> foundUsers = userDao.findByLastName("anotherName");
		assertEquals(1, foundUsers.size());
	}

	@Test
	public void findByEmail() {
		User user1 = createUser("firstUser", "mail@me.com");

		userDao.saveEntity(user1);

		User foundUser = userDao.findByEMail("mail@me.com");
		assertNotNull(foundUser);
	}

	@Test
	public void saveAddressCascadeTest() {
		User user = createUser("firstUser", "mail@me.com");
		Address address = createInfonovaAddress();
		user.setUserAddress(address);

		userDao.saveEntity(user);

		Long addressId = address.getId();
		String username = user.getUsername();
		assertNotNull(username);

		assertNotNull(em.find(User.class, username));
		userDao.deleteEntity(user);

		em.flush();
		em.clear();

		assertNull(em.find(User.class, username));
		assertNull(em.find(Address.class, addressId));
	}

	@Test
	public void listUserPreviews() {
		userDao.saveEntity(createBlogUser("thomas", "Thomas", "Offner",
				"thomas@mail.com"));
		userDao.saveEntity(createBlogUser("lubos", "Lubos", "Bistak",
				"lubos@mail.com"));

		List<UserPreview> userPreviews = userDao.listUserPreviews();
		assertEquals(2, userPreviews.size());

		UserPreview lubos = userPreviews.get(0);
		assertEquals("Lubos Bistak", lubos.getName());
		assertEquals("Lassallestraße 7a, 1020 Vienna", lubos.getAddress());

		UserPreview thomas = userPreviews.get(1);
		assertEquals("Thomas Offner", thomas.getName());
		assertEquals("Lassallestraße 7a, 1020 Vienna", thomas.getAddress());
	}

	private User createBlogUser(String username, String firstName,
			String lastName, String eMail) {
		User user = new User();
		user.setUsername(username);
		user.seteMail(eMail);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setUserAddress(createInfonovaAddress());
		return user;
	}

	private Address createInfonovaAddress() {
		Address address = new Address();
		address.setCity("Vienna");
		address.setPostalCode("1020");
		address.setStreet("Lassallestraße 7a");
		return address;
	}

	private User createUser(String primaryKey, String eMail) {
		User user = new User();
		user.setUsername(primaryKey);
		user.setFirstName("firstName");
		user.setLastName("lastName");
		user.seteMail(eMail);
		return user;
	}
}
