package com.infonova.devcafe.blog.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel( User.class )
public class Address_ {

  public static volatile SingularAttribute<Address, String> postalCode;

}
