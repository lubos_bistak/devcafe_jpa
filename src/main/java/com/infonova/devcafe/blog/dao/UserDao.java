package com.infonova.devcafe.blog.dao;

import java.util.List;

import com.infonova.devcafe.blog.model.User;
import com.infonova.devcafe.blog.model.UserPreview;

public interface UserDao extends AbstractDao<User, String> {

	List<User> findByFirstName(String firstName);

	List<User> findByLastName(String lastName);

	User findByEMail(String eMail);

	List<UserPreview> listUserPreviews();
}
