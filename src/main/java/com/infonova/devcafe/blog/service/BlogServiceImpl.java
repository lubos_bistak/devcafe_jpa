package com.infonova.devcafe.blog.service;

import com.infonova.devcafe.blog.service.BlogService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.infonova.devcafe.blog.dao.BlogDao;
import com.infonova.devcafe.blog.dao.UserDao;
import com.infonova.devcafe.blog.model.Blog;
import com.infonova.devcafe.blog.model.User;

@Service
public class BlogServiceImpl implements BlogService {

	@Autowired
	private BlogDao blogDao;

	@Autowired
	UserDao userDao;

	@Transactional
	public void addBlogEntry(String username, Blog blogEntry) {
		blogEntry.setUser(new User(username));
		blogEntry.setEntryTime(new DateTime());
		blogDao.saveEntity(blogEntry);
	}
}