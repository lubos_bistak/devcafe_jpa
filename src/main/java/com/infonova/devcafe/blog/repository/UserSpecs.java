package com.infonova.devcafe.blog.repository;

import com.infonova.devcafe.blog.model.Address_;
import com.infonova.devcafe.blog.model.User;
import com.infonova.devcafe.blog.model.User_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class UserSpecs {

  public static Specification<User> livesInAustria() {
    return new Specification<User>() {
      public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query,
                                   CriteriaBuilder builder) {

        return builder.equal(builder.length(root.get(User_.userAddress).get(Address_.postalCode)), 4);
      }
    };
  }

  public static Specification<User> livesInSlovakia() {
    return new Specification<User>() {
      public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query,
                                   CriteriaBuilder builder) {

        return builder.equal(builder.length(root.get(User_.userAddress).get(Address_.postalCode)), 5);
      }
    };
  }

}
