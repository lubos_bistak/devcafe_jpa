package com.infonova.devcafe.blog.repository;

import com.infonova.devcafe.blog.model.User;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends PagingAndSortingRepository<User, String>, JpaSpecificationExecutor {

	User findByUsername(String username);

  User findByFirstNameOrLastNameAllIgnoreCase(String firstName, String lastName);

  User findByUserAddressPostalCode(String postalCode);

  @Query("select u from User u where u.lastName like %?1")
  List<User> findByLastNameEndsWith(String suffix);

  @Query("select u from User u where u.eMail = :emailAddress")
  List<User> findByEmailAddress(@Param("emailAddress") String emailAddress);

}
