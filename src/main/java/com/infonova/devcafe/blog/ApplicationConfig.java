package com.infonova.devcafe.blog;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan("com.infonova.devcafe.blog")
@EnableJpaRepositories("com.infonova.devcafe.blog.repository")
@Import(PropertyPlaceholderConfig.class)
public class ApplicationConfig {

	@Value("${db.driverClassName}")
	private String driverClassName;

	@Value("${db.url}")
	private String url;

	@Value("${db.username}")
	private String username;

	@Value("${db.password}")
	private String password;

	@Value("${db.database}")
	private Database database;

	@Value("${db.gererateDDL}")
	private boolean generateDdl;

	@Value("${db.showSql}")
	private boolean showSql;

	@Bean
	public EntityManagerFactory entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setDataSource(dataSource());
		entityManagerFactory.setPackagesToScan(new String[] { "com.infonova.devcafe.blog.model" });
		// entityManagerFactory.setPersistenceProvider(new
		// HibernatePersistence());
		entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter());
		entityManagerFactory.afterPropertiesSet();
		return entityManagerFactory.getObject();
	}

	@Bean
	public DataSource dataSource() {
		// EmbeddedDatabaseFactoryBean ds = new EmbeddedDatabaseFactoryBean();
		// ds.setDatabaseType(EmbeddedDatabaseType.HSQL);
		// ds.afterPropertiesSet();
		// return ds.getObject();

    // return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).build();

		BasicDataSource ds = new org.apache.commons.dbcp.BasicDataSource();
		ds.setDriverClassName(driverClassName);
		ds.setUrl(url);
		ds.setUsername(username);
		ds.setPassword(password);
		return ds;
	}

	private HibernateJpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter jpaVendor = new HibernateJpaVendorAdapter();
		jpaVendor.setGenerateDdl(Boolean.valueOf(generateDdl));
		jpaVendor.setShowSql(Boolean.valueOf(showSql));
		jpaVendor.setDatabase(database);
		return jpaVendor;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory());
		transactionManager.setDataSource(dataSource());
		transactionManager.setJpaDialect(jpaDialect());
		return transactionManager;
	}

  @Bean
  public JpaDialect jpaDialect() {
    return new HibernateJpaDialect();
  }
}
